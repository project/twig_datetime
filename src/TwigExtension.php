<?php
namespace Drupal\twig_datetime;
use Twig\Extension\AbstractExtension;
use Drupal\Core\Datetime\DrupalDateTime;

class TwigExtension extends AbstractExtension {
 
  /**
   * {@inheritdoc}
   * This function must return the name of the extension. It must be unique.
   */
  public function getName() {
    return 'get_makeDateTime';
  }
 
  /**
   * In this function we can declare the extension function.
   */
  public function getFunctions() {
    return array(
      new \Twig\TwigFunction('get_datetime', array($this, 'get_datetime'), array('is_safe' => array('html'))),
    );
  }

	public static function get_datetime($datefield) {
		$date =  DrupalDateTime::createFromFormat('Y-m-d\TH:i:s', $datefield);
		$date->modify('+ 8 hour');
		$date->modify('-12 hour');
		$date->setTimezone(new \DateTimeZone('America/New_York'));
		
		// handle DST
		if (date('I', time())==0) {
			$date->modify('-1 hour');
		}
		
		return $date;
	}
  
}