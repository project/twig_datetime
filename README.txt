Twig DateTime

Drupal module to create a DateTime from given date using Twig.
Simply use the get_datetime() function with a date string as 
the lone parameter to get a formatted DateTime for calendars.